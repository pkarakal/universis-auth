import 'console.table';
import {finalize, getApplication} from "../../utils";


export const command = 'users';

export const desc = 'Lists users';

export function builder(yargs) {
    return yargs.option('dev', {
        default: false,
        describe: 'enables development mode'
    }).option('filter', {
        default: null,
        describe: 'defines query filter'
    }).option('top', {
        default: 25,
        describe: 'defines query top option'
    }).option('skip', {
        default: 0,
        describe: 'defines query skip option'
    });
}

export function handler(argv) {
    if (argv.dev) {
        process.env.NODE_ENV = 'development'
    }
    const app = getApplication();
    app.execute(async (context)=> {
        try {
            // get query options
            let query = Object.assign({ }, {
                $filter: argv.filter,
                $top: argv.top,
                $skip: argv.skip,
                $count: true
            });
            // get data queryable
            let getUsers = await context.model('User').filter(query);
            // get users
            let users = await getUsers.select('name', 'description', 'enabled', 'dateCreated').silent().getList();
            // print list
            if (users.value.length>0) {
                console.table(users.value);
            }
            // print footer
            console.log(`(Total records): ${users.total}`);
            // finalize context
            await finalize(context);
            // exit
            process.exit();
        }
        catch(err) {
            // finalize context
            await finalize(context);
            // log error
            console.error(err);
            // exit
            process.exit(-2);
        }
    });
}
