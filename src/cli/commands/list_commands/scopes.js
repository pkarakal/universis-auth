import 'console.table';
import {finalize, getApplication} from "../../utils";


export const command = 'scopes';

export const desc = 'Lists all authorization scopes';

export function builder(yargs) {
    return yargs.option('dev', {
        default: false,
        describe: 'enables development mode'
    });
}

export function handler(argv) {
    if (argv.dev) {
        process.env.NODE_ENV = 'development'
    }
    const app = getApplication();
    app.execute(async (context)=> {
        try {
            // get scopes
            let scopes = await context.model('AuthScope').asQueryable().silent().getAllItems();
            if (scopes.length > 0) {
                // print list
                console.table(scopes);
            }
            // print footer
            console.log(`(Total records): ${scopes.length}`);
            // finalize context
            await finalize(context);
            // exit
            process.exit();
        }
        catch(err) {
            // finalize context
            await finalize(context);
            // log error
            console.error(err);
            // exit
            process.exit(-2);
        }
    });
}
