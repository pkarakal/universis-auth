import path from 'path';

export const command = 'list <command>';

export const desc = 'Returns a list of objects (client, user, scope etc)';

export function builder(yargs) {
    return yargs.commandDir(path.resolve(__dirname, 'list_commands'));
}

export function handler() {

}
